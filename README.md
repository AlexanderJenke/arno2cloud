# arno2cloud
arno2cloud is a simple bot cloning events from the Malteser ARNO plattform to a caldav calendar.

## Caution
This bot deletes all events from the calendar before adding new ones. 
This is done to prevent duplicates. 
If you want to keep events in your calendar, you should create a new one.

## Installation
1. Clone the repository
2. Install the requirements with `pip install -r requirements.txt`
3. Edit `config.py` to your needs
4. Add a cronjob to run the script with `python arno2cloud.py` regularly 

## Configuration
The configuration is done in the `config.py` file. The following values are required:

* `arno_token`: The session token for the ARNO plattform
* `arno_session`: The session id for the ARNO plattform
* `caldav_url`: The url to the caldav calendar
* `caldav_username`: The username for the caldav calendar
* `caldav_password`: The password for the caldav calendar
* `caldav_calendar`: The name of the calendar to use

### Getting the ARNO session token and id
1. Login to the ARNO plattform (https://arno.cloud/)
2. Open the developer console
3. Go to the network tab
4. Reload the Events page
5. Search for a request to `https://arno.cloud/api/core/model/Event/getCalendarEvents`
6. Copy the `session_id` and `XSRF-TOKEN` cookies
7. Search the `divisionId` and `organizationId` in the **request** body
8. close the browser **withouth logging out **

For me the session stayed alive for over half a year now.
This might change in the future.