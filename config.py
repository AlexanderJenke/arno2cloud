# arno config, see README.md for how to get it
arno_token = "<session_token>"
arno_session = "<session_id>"
arno_division_id = "<division_id>"
arno_organization_id = "<organization_id>"

# caldav config
caldav_host = "<url>"
caldav_user = "<user>"
caldav_pwd = "<password>"
caldav_calendar = "<calendar>"
