from datetime import datetime, timedelta

import caldav
import requests

import config


def get_events() -> list:
    """
    Get events from arno.cloud
    :return: list of events
    """
    url = 'https://arno.cloud/api/core/model/Event/getCalendarEvents'
    data = {
        'q':
            {'options':
                 {'divisionId': [config.arno_division_id],  # get events for this division
                  'startDate': datetime.now().strftime("%Y-%m-%dT00:00:00"),
                  'endDate': (datetime.now() + timedelta(days=90)).strftime("%Y-%m-%dT00:00:00"),
                  # get events for next 90 days
                  'showArchived': False,  # don't show archived events
                  'previewDetails': True,  # get details
                  'attributes': ['id', 'Subject', 'StartDate', 'EndDate',
                                 'LocationName', 'MeetingPlaceName', 'MeetingTime'],
                  'fetchType': 'SERVICE'
                  }
             },
        'c': {'organizationId': config.arno_organization_id}  # get events for this organization
    }

    headers = {'x-xsrf-token': config.arno_token,
               'cookie': f'sessionID={config.arno_session}; XSRF-TOKEN={config.arno_token}'}  # set authentication headers

    r = requests.post(url, json=data, headers=headers)  # send request

    if r.status_code < 300:  # check if request was successful
        return r.json()['results']
    else:
        return []


def update_calendar(results: list):
    """Update calendar with new events. Deletes all old events!

    :param results: list of events
    :return: None
    """

    client = caldav.DAVClient(url=config.caldav_host, username=config.caldav_user,
                              password=config.caldav_pwd)  # connect to caldav server
    principal = client.principal()  # get principal
    cal = principal.calendar(config.caldav_calendar)  # get calendar

    for event in cal.events():  # delete all old events from calendar (we don't want to have duplicate events)
        event.delete()

    for r in results:  # add all new events
        caldav.Event()
        cal.save_event(
            dtstart=datetime.strptime(r['StartDate'], "%Y-%m-%dT%H:%M:%S.%fZ") + timedelta(hours=2),  # fix +2h for summer time
            dtend=datetime.strptime(r['EndDate'], "%Y-%m-%dT%H:%M:%S.%fZ") + timedelta(hours=2),  # todo: fix for winter time
            summary=r['Subject'],
            location=r['LocationName'],
        )
        print(r)

if __name__ == '__main__':
    arno_results = get_events()  # get results from arno.cloud
    if len(arno_results):  # only update calendar if there are results
        update_calendar(arno_results)
